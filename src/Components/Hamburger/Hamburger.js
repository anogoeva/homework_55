import React from 'react';

const Hamburger = props => {
    return (
        <div>
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"/>
                    <div className="Seeds2"/>
                </div>
                {props.hamburgerContent}
                <div className="BreadBottom"/>
            </div>
        </div>
    );
};

export default Hamburger;
