import "../../App.css";
import Meat from '../../assets/Meat.jpg';
import Salad from '../../assets/Salad.jpg';
import Cheese from '../../assets/Cheese.jpg';
import Bacon from '../../assets/Bacon.jpeg';

const Ingredients = props => {
    let imgVar = null;

    if (props.name === "Meat") {
        imgVar = Meat;
    } else if (props.name === "Cheese") {
        imgVar = Cheese;
    } else if (props.name === "Salad") {
        imgVar = Salad;
    } else imgVar = Bacon;

    return (
        <div className="ingredientsBox">
            <img src={imgVar} alt="#" width="40px" height="40px" onClick={props.onHeaderClick} className="ingredientsImg"/>
            <p><strong  className="ingredientsName">{props.name}</strong></p>
            <p><i className="ingredientsCount">{props.count}</i></p>
            <p>
                <button onClick={props.onRemove}>Delete</button>
            </p>
        </div>
    );
};

export default Ingredients;