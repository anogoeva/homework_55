import {useState} from "react";
import {nanoid} from 'nanoid';
import './App.css';
import Ingredients from "./Components/Ingredients/Ingredients";
import Hamburger from "./Components/Hamburger/Hamburger";

const App = () => {
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0, id: nanoid(), price: 50},
        {name: 'Cheese', count: 0, id: nanoid(), price: 20},
        {name: 'Salad', count: 0, id: nanoid(), price: 5},
        {name: 'Bacon', count: 0, id: nanoid(), price: 30},
    ]);

    const [totalPrice, setTotalPrice] = useState(20);
    const ingredientsComponents = ingredients.map(ingredient => (
        <Ingredients
            img={ingredient.image}
            key={ingredient.id}
            id={ingredient.id}
            name={ingredient.name}
            count={ingredient.count}
            onHeaderClick={() => increaseCount(ingredient.id)}
            onRemove={() => removeIngredient(ingredient.id)}
        />
    ));

    const increaseCount = id => {
        setIngredients(ingredients.map(ing => {
            if (ing.id === id) {
                setTotalPrice(totalPrice + ing.price);
                return {...ing, count: ing.count + 1};
            }
            return ing;
        }));
    };

    const removeIngredient = id => {
        setIngredients(ingredients.map(ing => {
            if (ing.id === id) {
                if (ing.count > 0) {
                    setTotalPrice(totalPrice - ing.price);
                    return {...ing, count: ing.count - 1};
                } else if (ing.count <= 0) {
                    setTotalPrice(20);
                    return {...ing, count: 0};
                }
            }
            return ing;
        }));
    };

    const hamburgerContent = () => {
        let burger = [];

        for (let i = 0; i < ingredients[0].count; i++) {
            burger.push(<div key={burger.length} className="Meat"/>);
        }
        for (let i = 0; i < ingredients[1].count; i++) {
            burger.push(<div key={burger.length} className="Cheese"/>);
        }
        for (let i = 0; i < ingredients[2].count; i++) {
            burger.push(<div key={burger.length} className="Salad"/>);
        }
        for (let i = 0; i < ingredients[3].count; i++) {
            burger.push(<div key={burger.length} className="Bacon"/>);
        }
        return burger;
    };

    return (
        <div className="hamburgerContainer">
            <fieldset className="ingredients">
                <legend className="title">Ingredients</legend>
                {ingredientsComponents}
            </fieldset>
            <fieldset>
                <legend className="title">Hamburger</legend>
                <Hamburger hamburgerContent={hamburgerContent()}/>
                <div>Price: {totalPrice} сом</div>
            </fieldset>
        </div>
    );
};
export default App;
